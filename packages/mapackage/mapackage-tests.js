// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by mapackage.js.
import { name as packageName } from "meteor/bardia:mapackage";

// Write your tests here!
// Here is an example.
Tinytest.add('mapackage - example', function (test) {
  test.equal(packageName, "mapackage");
});
