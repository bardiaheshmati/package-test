
import Comps from './Comps';

import React from 'react';
import { Route, Redirect } from 'react-router-dom';


//Import all your components here
const subMaPack = ()=>{
  return (
    <div>
      This is the sub pack
    </div>
  );
}

//Export Router and their components
const MaPack = ()=>{
  return(
    <div>
      <Route path='/maPack' exact={true} component={Comps} />
      <Route path='/maPack/:_id' exact={true} component={subMaPack} />
    </div>
  )
}


export default MaPack;
